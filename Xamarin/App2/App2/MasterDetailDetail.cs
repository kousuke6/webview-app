﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace App2
{
    public class MasterDetailDetail : ContentPage
    {
        public WebView webPage0;
        public WebView webPageTitle;
        public WebView webPage2;
        public Grid grid;
        public int nowWebViewNum;

        public MasterDetailDetail()
        {
            nowWebViewNum = 0;

            grid = new Grid
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                RowDefinitions = {
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },
                    new RowDefinition { Height = new GridLength(1, GridUnitType.Star) },

                },
                ColumnDefinitions = {

                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },
                    new ColumnDefinition { Width = new GridLength(1, GridUnitType.Star) },

                },

                RowSpacing = 0,
                ColumnSpacing = 0,
                Padding = new Thickness(0, Device.OnPlatform(15, 0, 0), 0, 0)


            };


            Button menuButton = new Button
            {
                Text = "MENU",
                Font = Font.SystemFontOfSize(NamedSize.Medium),
                WidthRequest = 70,
                HeightRequest = 50,
                BorderWidth = 1,
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.CenterAndExpand
            };


            //};
            menuButton.Clicked += delegate
            {
                App.masterDetail.IsPresented = true;

            };

            webPage0 = new WebView();
            webPage0.Source = "";

            webPageTitle = new WebView();
            webPageTitle.Source = "";



            //grid.Children.Add(label0, 0, 4, 0, 1);
            grid.Children.Add(webPageTitle, 0, 4, 0, 1);
            grid.Children.Add(webPage0, 0, 4, 1, 10);
            grid.Children.Add(menuButton, 0, 1, 0, 1);

            this.Content = grid;
        }
    }

}