﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace App2
{
    public class MenuItem
    {
        public string Title { get; set; }

        public string IconSource { get; set; }
        // この Type で移動先のページクラスを指定しています。
        public Type TargetType { get; set; }
        public string url { get; set; }
        public int webViewNum { get; set; }

    }
}