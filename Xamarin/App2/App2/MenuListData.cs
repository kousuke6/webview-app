﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace App2
{
    public class MenuListData : List<MenuItem>
    {
        public MenuListData()
        {
            this.Add(new MenuItem()
            {
                Title = "ブログ",
                IconSource = "leads.png",
                //TargetType = typeof(Detail1)
                webViewNum = 0,
                url = "http://"

            });

            this.Add(new MenuItem()
            {
                Title = "メニュー",
                IconSource = "contacts.png",
                //TargetType = typeof(Content1)
                webViewNum = 0,
                url = ""
            });


            this.Add(new MenuItem()
            {
                Title = "店舗情報",
                IconSource = "accounts.png",
                //TargetType = typeof(Detail1),
                webViewNum = 0,
                url = ""
            });

            this.Add(new MenuItem()
            {
                Title = "カナヘイの壁紙",
                IconSource = "accounts.png",
                //TargetType = typeof(Detail1),
                webViewNum = 0,
                url = "http://www."
            });

            this.Add(new MenuItem()
            {
                Title = "webPage1 SAI",
                IconSource = "opportunities.png",
                //TargetType = typeof(Detail1)
                webViewNum = 1,
                url = ""
            });

            this.Add(new MenuItem()
            {
                Title = "webPage2",
                IconSource = "opportunities.png",
                //TargetType = typeof(Detail1)
                webViewNum = 2,
                url = "https://"
            });
        }
    }
}