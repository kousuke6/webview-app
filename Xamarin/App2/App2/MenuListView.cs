﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace App2
{
    public class MenuListView : ListView
    {
        public MenuListView()
        {
            List<MenuItem> data = new MenuListData(); // インスタンス化して、
            ItemsSource = data; // ItemsSource として指定します。
            VerticalOptions = LayoutOptions.FillAndExpand;
            BackgroundColor = Color.Transparent;

            // ItemTemplate で使用しているのが ImageCell なので Android では Text が水色になってしまいます。
            // 嫌な場合は ImageCell ではなく ViewCell で ItemTemplate を作りましょう。
            var cell = new DataTemplate(typeof(ImageCell));
            cell.SetBinding(TextCell.TextProperty, "Title");
            cell.SetBinding(ImageCell.ImageSourceProperty, "IconSource");

            ItemTemplate = cell;
        }
    }
}