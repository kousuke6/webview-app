package red.---_s.---.user;

import android.app.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.widget.CompoundButton;

import com.google.firebase.messaging.FirebaseMessaging;

import org.xwalk.core.XWalkResourceClient;
import org.xwalk.core.XWalkView;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MainActivity extends Activity {

    // XWalkViewに登録するResourceClient
    private class ResourceClient extends XWalkResourceClient {
        ResourceClient(XWalkView view) {
            super(view);
        }

        // XWalkViewのロード完了通知
        @Override
        public void onLoadFinished(XWalkView view, String url) {
            super.onLoadFinished(view, url);
            // 描画後にグローバルに定義されたjavascript関数をコール
            xWalkView.evaluateJavascript("onLoaded();", null);
        }

        // Location変更通知
        @Override
        public boolean shouldOverrideUrlLoading(XWalkView view, String url) {
            // URLを使って値の受け渡しを行う
            if (url.contains("app-api://")) {
                // 'true'を返した場合Locationの変更がキャンセルされる
                return true;
            }

            // 'false'を返した場合Locationの変更が続行される
            return false;
        }
    }

    private XWalkView xWalkView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //FCM用に「user」トピックを作成
        FirebaseMessaging.getInstance().subscribeToTopic("---_user");

        ifHuaweiAlert();//Huawei対策

//        Log.d("abcd", "Refreshed token: ");

        // XWalkViewを取得
        xWalkView = (XWalkView) findViewById(R.id.webView);

        // XWalkViewから通知を受け取るためのXWalkResourceClient継承クラスをset
        xWalkView.setResourceClient(new ResourceClient(xWalkView));
        xWalkView.setUserAgentString("---_user");

        // loadメソッドに'Map<String, String>'を渡すことでリクエストヘッダーにフィールドを追加することができる
        Map<String, String> additionalHttpHeaders = new HashMap<>();
        additionalHttpHeaders.put("mobileAppAuth", "3f7d22402f6ac375bf1168c3ef5d8f08");

        //localhostはアクセスできないのでローカルサーバーで開発中は自分のIPアドレス,ポートを指定してね
        xWalkView.load("https://---.php", null);

        //***********************************************************
        // ここでグローバルに定義されたjavascript関数をコールしても
        // `is not defined エラーで怒られます`
        //***********************************************************
        // xWalkView.evaluateJavascript("onLoaded();", null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (xWalkView != null) {
            xWalkView.pauseTimers();
            xWalkView.onHide();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (xWalkView != null) {
            xWalkView.resumeTimers();
            xWalkView.onShow();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (xWalkView != null) {
            xWalkView.onDestroy();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (xWalkView != null) {
            xWalkView.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        if (xWalkView != null) {
            xWalkView.onNewIntent(intent);
        }
    }


//Huawei対策
    private void ifHuaweiAlert() {
        final SharedPreferences settings = getSharedPreferences("ProtectedApps", MODE_PRIVATE);
        final String saveIfSkip = "skipProtectedAppsMessage";
        boolean skipMessage = settings.getBoolean(saveIfSkip, false);
        if (!skipMessage) {
            final SharedPreferences.Editor editor = settings.edit();
            Intent intent = new Intent();
            intent.setClassName("com.huawei.systemmanager", "com.huawei.systemmanager.optimize.process.ProtectActivity");
            if (isCallable(intent)) {
                final AppCompatCheckBox dontShowAgain = new AppCompatCheckBox(this);
                dontShowAgain.setText("既にチェック済み");
                dontShowAgain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        editor.putBoolean(saveIfSkip, isChecked);
                        editor.apply();
                    }
                });

                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setTitle("はじめに")
                        .setMessage(String.format("アプリを快適に動作させるため、\n下の 保護アプリ一覧 から\n\n「%sアプリにチェック」\n\nをつけてください", getString(R.string.app_name)))
                        .setView(dontShowAgain)
                        .setPositiveButton("保護アプリ一覧", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                huaweiProtectedApps();
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, null)
                        .show();
            } else {
                editor.putBoolean(saveIfSkip, true);
                editor.apply();
            }
        }
    }

    private boolean isCallable(Intent intent) {
        List<ResolveInfo> list = getPackageManager().queryIntentActivities(intent,
                PackageManager.MATCH_DEFAULT_ONLY);
        return list.size() > 0;
    }

    private void huaweiProtectedApps() {
        try {
            String cmd = "am start -n com.huawei.systemmanager/.optimize.process.ProtectActivity";
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                cmd += " --user " + getUserSerial();
            }
            Runtime.getRuntime().exec(cmd);
        } catch (IOException ignored) {
        }
    }

    private String getUserSerial() {
        //noinspection ResourceType
        Object userManager = getSystemService("user");
        if (null == userManager) return "";

        try {
            Method myUserHandleMethod = android.os.Process.class.getMethod("myUserHandle", (Class<?>[]) null);
            Object myUserHandle = myUserHandleMethod.invoke(android.os.Process.class, (Object[]) null);
            Method getSerialNumberForUser = userManager.getClass().getMethod("getSerialNumberForUser", myUserHandle.getClass());
            Long userSerial = (Long) getSerialNumberForUser.invoke(userManager, myUserHandle);
            if (userSerial != null) {
                return String.valueOf(userSerial);
            } else {
                return "";
            }
        } catch (NoSuchMethodException | IllegalArgumentException | InvocationTargetException | IllegalAccessException ignored) {
        }
        return "";
    }
    //////////////////Huawei対策









}
