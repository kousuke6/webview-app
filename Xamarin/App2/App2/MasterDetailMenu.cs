﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace App2
{
    public class MasterDetailMenu : ContentPage
    {
        public ListView Menu { get; set; }

        public MasterDetailMenu()
        {
            Icon = "icon.png"; // Icon を設定すると左側が文字でなくアイコンで置き換わります。
            Title = "Menu"; // Icon を指定しても Title プロパティは必須項目です。
            BackgroundColor = Color.FromHex("dce8ef");


            // ListView 設定
            Menu = new MenuListView();

            var menuLabel = new ContentView
            {
                Padding = new Thickness(10, 36, 0, 5),
                Content = new Label
                {
                    TextColor = Color.FromHex("333"),
                    Text = "メニュー",
                    FontSize = 18,
                }
            };

            // タイトルの menuLabel と ListView を並べています。
            var layout = new StackLayout
            {
                Spacing = 0,
                VerticalOptions = LayoutOptions.FillAndExpand
            };
            layout.Children.Add(menuLabel);
            layout.Children.Add(Menu);

            Content = layout;
        }
    }

}