﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace App2
{
    public class MasterDetail : MasterDetailPage
    {
        public MasterDetail()
        {

            var menuPage = new MasterDetailMenu();
            // Menu ページの ListView Menu を選択した時に NavigateTo メソッドに SelectedItem を渡します。
            //menuPage.Menu.ItemSelected += (sender, e) => NavigateTo(e.SelectedItem as MenuItem);
            menuPage.Menu.ItemTapped += (sender, e) => NavigateTo(e.Item as MenuItem);
            Master = menuPage;

            // Detail は NavigationPage で呼び出した方が良いです。(左からのスワイプで Master を出せるが操作しづらい)
            // バーの色を変えています。

            //var detail = new Content1();
            var detail = App.masterDetailDetail;
            //var detail2 = new ImgPage1();


            //var detail = new NavigationPage(new Tekitou2());           
            //detail.BarBackgroundColor = Color.FromHex("3498DB");
            //detail.BarTextColor = Color.White;




            //Detail = new NavigationPage(new masterDetailDetail());
            Detail = detail;





            /// <summary>
            /// ページ遷移のメソッドです。
            /// </summary>
            /// <param name="menu">MenuItem</param>
            void NavigateTo(MenuItem menu)
            {
                // menuPage の List<MenuItem> の選択値を MenuItem で受け取っているので
                // 予め定義されたページに移動できるってことは分かるんですが、凄いですね。
                //Page displayPage = (Page)Activator.CreateInstance(menu.TargetType);

                //// 同じく各ページに移動する時にもバーの色を再設定 (このやり方では必須)
                //var detail = new NavigationPage();
                //detail.BarBackgroundColor = Color.FromHex("3498DB");
                //detail.BarTextColor = Color.White;
                //Detail = (Page)Activator.CreateInstance(menu.TargetType);




                if (menu.webViewNum != App.masterDetailDetail.nowWebViewNum)
                {
                    if (menu.webViewNum == 0)
                    {
                        App.masterDetailDetail.grid.Children.Remove(App.masterDetailDetail.webPageTitle);
                        App.masterDetailDetail.grid.Children.Remove(App.masterDetailDetail.webPage2);
                        App.masterDetailDetail.grid.Children.Add(App.masterDetailDetail.webPage0, 0, 4, 1, 10);
                        App.masterDetailDetail.webPage0.Source = menu.url;

                        App.masterDetailDetail.webPage0.Navigated += delegate
                        {
                            IsPresented = false;
                        };

                        App.masterDetailDetail.nowWebViewNum = 0;
                    }

                    else if (menu.webViewNum == 1)
                    {
                        App.masterDetailDetail.grid.Children.Remove(App.masterDetailDetail.webPage0);
                        App.masterDetailDetail.grid.Children.Remove(App.masterDetailDetail.webPage2);
                        App.masterDetailDetail.grid.Children.Add(App.masterDetailDetail.webPageTitle, 0, 4, 1, 10);

                        App.masterDetailDetail.webPageTitle.Navigated += delegate
                        {
                            IsPresented = false;
                        };

                        App.masterDetailDetail.nowWebViewNum = 1;
                    }

                    else if (menu.webViewNum == 2)
                    {
                        App.masterDetailDetail.grid.Children.Remove(App.masterDetailDetail.webPage0);
                        App.masterDetailDetail.grid.Children.Remove(App.masterDetailDetail.webPageTitle);
                        App.masterDetailDetail.grid.Children.Add(App.masterDetailDetail.webPage2, 0, 4, 1, 10);

                        App.masterDetailDetail.webPage2.Navigated += delegate
                        {
                            IsPresented = false;
                        };

                        App.masterDetailDetail.nowWebViewNum = 2;
                    }

                }
                else if (menu.webViewNum == App.masterDetailDetail.nowWebViewNum)
                {
                    if (menu.webViewNum == 0)
                    {
                        App.masterDetailDetail.webPage0.Source = menu.url;
                        App.masterDetailDetail.webPage0.Navigated += delegate
                        {
                            IsPresented = false;
                        };
                    }
                    else if (menu.webViewNum == 1)
                    {
                        App.masterDetailDetail.webPageTitle.Source = menu.url;
                        App.masterDetailDetail.webPageTitle.Navigated += delegate
                        {
                            IsPresented = false;
                        };
                    }
                    else if (menu.webViewNum == 2)
                    {
                        App.masterDetailDetail.webPage2.Source = menu.url;

                        App.masterDetailDetail.webPage2.Navigated += delegate
                        {
                            IsPresented = false;
                        };
                    }

                }
            }
        }
    }
}